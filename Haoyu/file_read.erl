-module(file_read).
-export([readlines/1, decode_json/1]).
-import(mochijson2, [decoder/1, decode/1, decode/2]).
 
readlines(FileName) ->
    {ok, Device} = file:open(FileName, [read]),
    get_all_lines(Device, []).
 
get_all_lines(Device, Accum) ->
    case io:get_line(Device, "") of
        eof  -> file:close(Device), Accum;
        Line -> get_all_lines(Device, Accum ++ [Line])
    end.

decode_json(FileName) ->
      mochijson2:decode(readlines(FileName)).