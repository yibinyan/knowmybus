-module(get_bus).
-export([get_body/1]).

get_body(URL) ->
    inets:start(),
    Response = httpc:request(get, {URL, []}, [], []),
    response_body(Response).

response_body({ok, { _, _, Body}}) -> Body.

% Response = httpc:request(get, {"http://truetime.portauthority.org/bustime/api/v2/gettime?key=FSSiQjCnXR7fwVLA6XKXTrav4&format=json", []}, [], []).
% response_body({ok, { _, _, Body}}) -> Body.
% Body = response_body(Response).
% io:format("~s~n",[Body]).

