-module(groupCommunicate).

-export([msgReceive/0, sendMessage/2, sendGroupMessage/2, startMsgReceiver/0, 
					start/1, getAllGroupMembers/0, sendToGroup/1,
						createTableAndInsert/0, insertToTable/2]).

-record(userInfo, {name, info = []}).

startMsgReceiver() ->
	mnesia:start(),
	mnesia:wait_for_tables([userInfo], 2000),
	register('MsgReceiver', spawn(groupCommunicate, msgReceive, [])).

msgReceive() ->
	receive
		{Message, Message_from} -> io:format("~p said:~p~n", [Message_from, Message]);
		Message -> io:format("~p ~n", [Message])
	end,

msgReceive().

sendMessage(Message, {Name, Node}) ->
	{Name, Node} ! {Message, node()},
	io:format("Message: ~p sent to ~p ~n", [Message, {Name, Node}]).
	


%Send Group Message
sendToGroup(Message) ->
	GroupOneMember = getAllGroupMembers(),
	sendGroupMessage(Message, GroupOneMember).
	

%Get info of all members in the same group.
getAllGroupMembers() ->
	Iterator = fun(Rec,Acc) ->
			[Rec | Acc]
			%lists:append(result, Rec),
			%io:format("~p ~p ~n",[Rec]),	
		   end,
	
	case mnesia:is_transaction() of
	    true -> mnesia:foldl(Iterator,[],userInfo);
	    false ->
	        Exec = fun({Fun, Tab}) -> mnesia:foldl(Fun,[],Tab) end,
	        mnesia:activity(transaction,Exec,[{Iterator, userInfo}], mnesia_frag)
	end.


%Send to each of nodes in the group
sendGroupMessage(Message, []) ->
	ok;

sendGroupMessage(Message, [Peer | RestPeers]) ->
	PeerAddr = {'MsgReceiver', Peer#userInfo.name},
	PeerAddr ! {Message, node()},
	io:format("Message: ~p sent to ~p ~n", [Message, Peer#userInfo.name]),
	sendGroupMessage(Message, RestPeers).


start(User_name) ->
	register(User_name, spawn(groupCommunicate, msgReceive, [])).



%%%%%%%%%%%%%%%%%%%%Just For Test%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
createTableAndInsert() ->
		mnesia:create_schema([node()]),
		mnesia:start(),
	        mnesia:create_table(userInfo,
					[{attributes, record_info(fields, userInfo)},
						{disc_copies, [node()]},
							{local_content, true}]),
		io:format("Table Create!"),
		
		F = fun() ->
			mnesia:write(#userInfo{name=node(), info=""})
		end,
		mnesia:activity(transaction, F).

insertToTable(InputName, InputInfo) ->
		mnesia:start(),
		mnesia:wait_for_tables([userInfo], 2000),
		
		F = fun() ->
			mnesia:write(#userInfo{name=InputName, info=InputInfo})
		end,
		mnesia:activity(transaction, F).

	
%sendToGroup(Message) ->
	% use RPC to get Member of group. call(Node, Module, Function, Args)
		
%	GroupOneMember = [{'Joyce', 'Joyce@yanyibins-mbp'}, {'Rohith', 'Rohith@yanyibins-mbp'}],
%	GroupTwoMember = [{'Haoyu', 'Haoyu@yanyibins-mbp'}],
		
%	if 
%		GroupName == 'GroupOne' ->
%			sendGroupMessage(Message, GroupOneMember);
%		GroupName == 'GroupTwo' ->
%			sendGroupMessage(Message, GroupTwoMember);
%		true ->
%			io:format("Group Not Found")
%	end.


