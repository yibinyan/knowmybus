-module(bootstrap_sup).
-behaviour(supervisor).

-export([start_link/0, init/1]).

%% Start supervisor and call init function
start_link() ->
	supervisor:start_link({global, ?MODULE}, ?MODULE, []).

%% Init bootstrap server node
init([]) ->
	{ok, {{one_for_one, 1, 10},				%% Single server, one failure every 10 seconds
		  [{bootstrap_id,					%% ChildID
		   {bootstrapServ, start_link, []},	%% Start function in {M, F, A} format
		   permanent,						%% Permanent node
		   5000,							%% Shutdown time of 5 sec
		   worker,							%% Worker node
		   [bootstrapServ]					%% Name of the callback Module
		 }]}}.