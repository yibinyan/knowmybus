-module(bootstrapServ).
-behaviour(gen_server).

-export([start_link/0, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-export([install/1, join_group/3, exit_group/2, traverse_table_and_show/0]).
-export([stateChecker/0, checkMembers/1, checkNodes/0]).

-define(INTERVAL, 15000).  % 20 seconds

%% ========================================================================================
%% Include files
%% ========================================================================================
-include_lib("stdlib/include/ms_transform.hrl").
-include_lib("stdlib/include/qlc.hrl").


%% ========================================================================================
%% Include files
%% ========================================================================================
%% Record storing member details in Mnesia table
-record(memberInfo, {busNum,
					 name,
					 info=[]}).


%% ========================================================================================
%% Basic init support
%% ========================================================================================
%% Start bootstrap server
start_link() ->
	gen_server:start_link({global, ?MODULE}, ?MODULE, [], []).


%% Stop bootstrap server functionality
stop() -> 
	%% Stop Mnesia if running
	mnesia:stop(),
	gen_server:call({global, ?MODULE}, stop).


%% ========================================================================================
%% Callbacks
%% ========================================================================================
init([]) ->
	spawn(bootstrapServ, stateChecker, []),
	%% Wait for table to get populated (remotely)
	mnesia:wait_for_tables([memberInfo], 2000),
	{ok, []}.


handle_call(_Call, _From, State) ->
    {noreply, State}.


handle_cast(_Cast, State) ->
    {noreply, State}.


handle_info(_Info, State) ->
    {noreply, State}.


code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


terminate(_Reason, _State) ->
    ok.

%% ========================================================================================
%% Global functions
%% ========================================================================================
%% Setup Mnesia table
install(Nodes) ->
	%% Create schema before creating table
	ok = mnesia:create_schema(Nodes),
	%% Mnesia needs to be started and stopped on multiple nodes
	rpc:multicall(Nodes, application, start, [mnesia]),
	%% Create table to keep track of all members belonging to (not local content)
	mnesia:create_table(memberInfo,
			[{attributes, record_info(fields, memberInfo)},  %% List of items in form [key, value]
			 {index, [#memberInfo.name]},					 %% Search with record fields other than primary
			 {disc_copies, Nodes},							 %% Store tables in disk and have copies in Nodes
			 {type, bag}]),									 %% Multiple entries with same key
	rpc:multicall(Nodes, application, stop, [mnesia]),
	io:format("~n Install complete ~n").


%% Allow member to join a group
join_group(BusNum, Name, Info) ->
	%% Insert member into Mnesia table
	insert_member(BusNum, Name, Info),
	%% Send info to all members in list
	send_newMemberInfo(BusNum, {Name, Info}),
	%% Send details of members in route to caller
	send_allMemberInfo(Name, BusNum).


%% Exit member from group
exit_group(BusNum, Name) ->
	%% Find member and delete from Mnesia table
	Delete = #memberInfo{busNum=BusNum, name=Name, info="blah"},
	Fun = fun() ->
		List = mnesia:match_object(Delete),
		lists:foreach(fun(X) ->
				mnesia:delete_object(X)
				end, List),
		%% Remove member from rest of the nodes
		remove_member(BusNum, Name)
	end,
	mnesia:activity(transaction, Fun).


%% ========================================================================================
%% Private functions
%% ========================================================================================
%% Find member and remove from group
find_and_removeMember(Name) ->
        % Search for the pattern containing Name
	Pattern = #memberInfo{_='_', name=Name, _='_'},
	F = fun() ->
		%% Match all objects having pattern
		Res = mnesia:match_object(Pattern),
		%io:format("Res: ~p~n", [Res]),
		if Res =:= [] ->
		    ok;
          true ->
            %io:format("Delete: ~p ~n", Res),
            lists:foreach(fun(X) ->
			              mnesia:delete_object(X)
			              end, Res)
		end
	end,
	{atomic, _} = mnesia:transaction(F).


%% Insert memeber into Mnesia table
insert_member(BusNum, Name, Info) ->
	%% Remove member if already in any list
	find_and_removeMember(Name),
	F = fun() ->
		mnesia:write(#memberInfo{busNum=BusNum, name=Name, info=Info})
	end,
	mnesia:activity(transaction, F).


%% send member details to caller
send_allMemberInfo(Name, BusNum) ->
	F = fun() ->
		Res = mnesia:read({memberInfo, BusNum}),
		%% Reponse is list of nodes
		List = [{N,I} || #memberInfo{name=N, info=I} <- Res],
		make_rpc(Name, user_app, add_allUsers, [List])
	end,
	mnesia:activity(transaction, F).


%% Send new member information to all nodes in group
send_newMemberInfo(BusNum, {Name, Info}) ->
	F = fun() ->
		Res = [{N,I} || #memberInfo{name=N, info=I} <- mnesia:read({memberInfo, BusNum})],
		sendInfo(Res, {Name, Info})
	end,
	mnesia:activity(transaction, F).


sendInfo([], _) ->
	ok;
sendInfo([{N, _} | Rest], {Name, Info}) ->
	if N =:= Name ->
		sendInfo(Rest, {Name, Info});
	  true ->
		make_rpc(N, user_app, add_newUser, [{Name, Info}]),
		sendInfo(Rest, {Name, Info})
	end.


%% Remove member from bus group
remove_member(BusNum, Member) ->
	Res = [{N,I} || #memberInfo{name=N, info=I} <- mnesia:read({memberInfo, BusNum})],
	removeInfo(Res, Member).

removeInfo([], _) ->
	ok;
removeInfo([{N, _} | Rest], Member) ->
	make_rpc(N, user_app, remove_user, [Member]),
	removeInfo(Rest, Member).


%% ========================================================================================
%% Fault tolerance
%% ========================================================================================
%% Check if nodes are still alive in frequent intervals
stateChecker() ->
	timer:apply_after(?INTERVAL, bootstrapServ, checkNodes, []).

checkNodes() ->
	Fun = fun() ->
		qlc:e(
			qlc:q([N || #memberInfo{busNum=_, name=N, info=_} <- mnesia:table(memberInfo)])
			)
	end,
	{atomic, UserInfoList} = mnesia:transaction(Fun),
	io:format(" 	LIST: ~p ~n", [UserInfoList]),
	checkMembers(UserInfoList),
	% Restart timer
	timer:apply_after(?INTERVAL, bootstrapServ, checkNodes, []).

%% Traverse the list of nodes and check their presence in Mnesia table
%% If node not alive, remove it
checkMembers([]) ->
	ok;
checkMembers([Node | Rest]) ->
	case lists:member(Node, nodes()) of
	      true ->
		io:format("			Member: ~p FOUND ~n", [Node]),
		ok;
	      false ->
		io:format("	Member ~p NOT FOUND ~n", [Node]),
		find_and_removeMember(Node)
	end,
	checkMembers(Rest).


%% Fault tolerant RPC call
make_rpc([], _, _, _) ->
	ok;
make_rpc(Node, ClientModule, ClientFunction, Args) ->
	try rpc:call(Node, ClientModule, ClientFunction, Args) of 
		{badrpc, _} ->
			%% Try again
			rpc:call(Node, ClientModule, ClientFunction, Args);
		_ ->
			ok
	catch
		_ -> ok
	end.


%% ========================================================================================
%% Debug Support
%% ========================================================================================
%% Print out table contents
traverse_table_and_show()->
    Iterator =  fun(Rec,_)->
                    io:format("~p~n",[Rec]),
                    []
                end,
    case mnesia:is_transaction() of
        true -> mnesia:foldl(Iterator,[],memberInfo);
        false -> 
            Exec = fun({Fun,Tab}) -> mnesia:foldl(Fun, [],Tab) end,
            mnesia:activity(transaction,Exec,[{Iterator,memberInfo}],mnesia_frag)
    end.
