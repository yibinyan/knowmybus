-module(bootstrap).
-behaviour(application).

-export([start/2, stop/1]).

%%%%%%%%%%%%%%%%%
%%% CALLBACKS %%%
%%%%%%%%%%%%%%%%%
start(normal, []) ->
	bootstrap_sup:start_link();
%% Use this in case of takeover by Main node
start({takeover, _OtherNode}, []) ->
	bootstrap_sup:start_link().


stop(_State) ->
	%% Stop the server normally
	ok.
