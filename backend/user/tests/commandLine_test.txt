Final rebar release build method:
1) cd to knowmybus/backend/user directory
2) Make sure there is nothing in the deps/ folder
3) Get dependencies using:
	./rebar get-deps
4) Create rel folder
	mkdir rel
5) Chage directory to rel and execute:
	cd rel
	../rebar create-node nodeid=user
5) Copy reltool.config from knowmybus/backend/setup/reltool.config to knowmybus/backend/user/rel/reltool.config
6) Copy vm.args from knowmybus/backend/setup/vm.args to knowmybus/backend/user/rel/files/vm.args
7) Change the IP address in knowmybus/backend/user/rel/files/vm.args to your IP address

8) Execute following command in knowmybus/backend/user
	./rebar compile generate
9) Execute the following command from path knowmybus/backend/user to run backend as background job
	./startUser.sh
	Use ./stopUser.sh to stop the User daemon
10) Open a browser and type
	a)  You will see this output: Leptus 0.4.2 started on ________________
		On my computer, it was: http://127.0.0.1:8080
	b)  Go to your browser and enter:
		http://127.0.0.1:8080/views/welcome_index.html
		(so basically the url + view/welcome_index.html)

Once setup is complete use the URL to use the application

DEBUG:
To run code in foreground replace step 9 with following execution:
		./startUser_debug.sh

—————————————————————————————————————————————————————————————————————————————————————————————————————
Manual rebar release test from console:
Run in terminal:
	1) erl -name user1@IP-ADDRESS -mnesia dir '"./Mnesia_user1"' -pa ebin/ deps/*/ebin
	2) application:start(ranch), application:start(cowlib), application:start(crypto), application:start(cowboy), application:start(leptus).
	3) application:start(mnesia), application:start(user).


Basic test:
—————————————————————————————————————————————————————————————————————————————————————————————————————
1) cd to knowmybus/backend/user directory
2) Compile using: 
	erl -make
3) Run on each instance using:
	erl -name aaa -pa ebin/
	or
	erl -sname aaa -pa ebin/

	Where sname is shortname and name is long name. Replace 'aaa' with name of your choice

—————————————————————————————————————————————————————————————————————————————————————————————————————
Run on each user:

userNode:install().
application:start(mnesia), application:start(user_worker).

Start test with:
userNode:select_busRoute("61D", "blah").

—————————————————————————————————————————————————————————————————————————————————————————————————————
GUI Test:
1) cd to knowmybus/backend/user directory
2) Make sure there is nothing in the deps/ folder
3) Get dependencies using:
	./rebar get-deps
4) Compile using:
	./rebar compile
5) Run an instance using:
	erl -name user1@IP-ADDRESS -mnesia dir '"./Mnesia_user1"' -pa ebin/ deps/*/ebin
6) Run the following commands:
	userNode:install().
	application:start(mnesia), application:start(user).
7) You will see this output: Leptus 0.4.2 started on ________________
	On my computer, it was: http://127.0.0.1:8080
8) Go to your browser and enter:
	http://127.0.0.1:8080/views/welcome_index.html
	(so basically the url + view/welcome_index.html)
9) Click “I know which bus I want”
10) Select a bus
	You will see that the fields are empty, since the P2P connection isn’t set up yet—this is just
	So you can see what the GUI looks like
11) Click “On Bus” on the top-right corner of the navigation bar
	You can enter information here, and then click the Submit button
12) Go back one page (just hit the back button on the browser)
	Hit refresh and you should see the data you just submitted!
	
—————————————————————————————————————————————————————————————————————————————————————————————————————

Issues to resolve:

1) Close Mnesia table while exiting -> Not happening with mnesia:del_table_copy(userInfo, node())
									or with mnesia:delete_table(userInfo).
2) Close app button to shutdown. From gui_request_handler.erl, it needs to call userNode.stop(BusNum) with bus number as parameter.
3) What happens if all bootstrap servers are down?
4) What happens if one of the user node is down - Can nodes() detect all possible cases before a crash
5) Need no-ip support for bootstrap server.
6) Can run only one application on a single machine
