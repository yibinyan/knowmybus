-module(groupCommunication).

-export([msgReceive/0, sendMessage/2, sendGroupMessage/2, startMsgReceiver/0, 
					  sendToGroup/1, holdAndRetry/2, tryAgain/4,
						createTableAndInsert/0, insertToTable/2]).
-include_lib("stdlib/include/qlc.hrl").
-record(userInfo, {name, info = []}).

startMsgReceiver() ->
	register('MsgReceiver', spawn(groupCommunication, msgReceive, [])).

msgReceive() ->
	receive
		{Message, _} -> 
			userNode:insert_BusRecord(Message);
		Message -> io:format("~p ~n", [Message])
	end,

msgReceive().


sendMessage(Message, {Name, Node}) ->
	{Name, Node} ! {Message, node()}.
	

%Send Group Message
sendToGroup(Message) ->
	Fun = fun() ->
		qlc:e(
			qlc:q([N || #userInfo{name=N, info=_} <- mnesia:table(userInfo)])
              		)
	end,
	{atomic, UserInfoList} = mnesia:transaction(Fun),
		
	sendGroupMessage(Message, UserInfoList),
	{ok}.


%Send to each of nodes in the group
sendGroupMessage(_, []) ->
	{ok};

sendGroupMessage(Message, [Peer | RestPeers]) ->
	
	if 
	   Peer =:= node() ->
		sendGroupMessage(Message, RestPeers);
	   true ->
		case lists:member(Peer, nodes()) of
		      true ->
				PeerAddr = {'MsgReceiver', Peer},
				PeerAddr ! {Message, node()},
				sendGroupMessage(Message, RestPeers);

		      false ->	
				spawn(groupCommunication, holdAndRetry, [Peer, Message]),
				sendGroupMessage(Message, RestPeers)
		end						
	end.


holdAndRetry(Peer, Message) ->
	timer:apply_after(20000, groupCommunication, tryAgain, [Peer, Message, 0, 5]).	


tryAgain(Peer, Message, TryCount, TotalTry) ->
	%TryCount = TryCount + 1,

	case lists:member(Peer, nodes()) of
	      true ->
			PeerAddr = {'MsgReceiver', Peer},
			PeerAddr ! {Message, node()},
			io:format("<Hold And Retry> Successfully, Message: ~p sent to ~p ~n", [Message, Peer]);

	      false ->	
			if
				TryCount+1 < TotalTry ->
				    io:format("<Hold And Retry> Try to send to ~p the ~p times ~n", [Peer, TryCount+1]),
			      	    userNode:removeUser(Peer),
			            timer:apply_after(2000, groupCommunication, tryAgain, [Peer, Message, TryCount+1, TotalTry]);
				true ->
				    io:format("<Hold And Retry> Fail, Remote client ~p is dead ~n", [Peer])
			end	
	end.
	


%%%%%%%%%%%%%%%%%%%%Just For Test%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
createTableAndInsert() ->
		mnesia:create_schema([node()]),
		mnesia:start(),
	        mnesia:create_table(userInfo,
					[{attributes, record_info(fields, userInfo)},
						{disc_copies, [node()]},
							{local_content, true}]),
		
		F = fun() ->
			mnesia:write(#userInfo{name=node(), info=""})
		end,
		mnesia:activity(transaction, F).

insertToTable(InputName, InputInfo) ->
		mnesia:start(),
		mnesia:wait_for_tables([userInfo], 2000),
		
		F = fun() ->
			mnesia:write(#userInfo{name=InputName, info=InputInfo})
		end,
		mnesia:activity(transaction, F).

