%% file: src/bus_record.hrl
%% -------------------------
-record(bus_records, {
   id,
   bus_name,
   latest_arrival_timestamp,
   latest_arrival_place,
   notifications_arrival,
   notifications_delay,
   notifications_early,
   fullness,
   smelliness,
   comments
}).
