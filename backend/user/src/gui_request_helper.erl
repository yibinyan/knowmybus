%% file src/gui_request_helper.erl
%% ------------------------
-module(gui_request_helper).
-export([format/1]).

format(List) -> format(List, []).
format([], Results) -> Results;
format([H|T], Results) -> format(T, [json(H)|Results]).

json({_, Key, Bus_Name, Latest_Arrival_Time, Latest_Arrival_Place, Notifications_Arrival, Notifications_Delay, Notifications_Early, Fullness, Smelliness, Comments}) ->
   {Key, [Bus_Name, Latest_Arrival_Time, Latest_Arrival_Place, Notifications_Arrival, Notifications_Delay, Notifications_Early, Fullness, Smelliness, Comments]}.