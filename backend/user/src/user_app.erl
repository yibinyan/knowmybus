-module(user_app).
-behaviour(application).

-export([start/2, stop/1, add_newUser/1, remove_user/1, add_allUsers/1]).


start(normal, []) ->
    userNode:install(),
	user_supervisor:start_link().

stop(_) ->
	ok.

%% Add a new member in the group
add_newUser({Name, Info}) ->
	userNode:addUser(Name, Info).

remove_user(Name) ->
	userNode:removeUser(Name).

add_allUsers(List) ->
	userNode:addAllUsers(List).
