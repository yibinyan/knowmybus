%% file: src/gui_request_handler.erl
%% ----------------------------------------------------
-module(gui_request_handler).
-compile({parse_transform, leptus_pt}).

%% Leptus callbacks
-export([init/3]).
-export([cross_domains/3]).
-export([terminate/4]).

%% Leptus function routes (GET and POST)
-export([get/3]).
-export([post/3]).

%% Includes
-include("bus_record.hrl").
-include_lib("stdlib/include/qlc.hrl").

%% Cross Domain Origin: Configured to accept any host for cross-domain requests
cross_domains(_Route, _Req, State) ->
    {['_'], State}.

%% Start
init(_Route, _Req, State) ->
    {ok, State}.

%% GET REQUEST for bus data information (List)
get("/users", _Req, State) ->
   RecordList = userNode:listify_BusRecords(),
   %% TODO: Use the RecordList to update info correctly
   Json = gui_request_helper:format(RecordList),
   {200, {json, Json}, State};

%% Request that handles passing BusNum to the bootstrap server
get("/user/:busnum", Req, State) ->
	BusNum = leptus_req:param(Req, busnum),
    io:format("Bus name~s~n", [BusNum]),
	Info = "blah",

    %% Call select_busRoute function from userNode
	Ret = userNode:select_BusRoute(BusNum, Info),
    %% Return with success (200) HTTP status code
    {Ret, {json, []}, State};

%% Request that handles exiting the program
get("/exit/:busnum", Req, State) ->
    %% Get bus number
	BusNum = leptus_req:param(Req, busnum),

    %% Call select_busRoute function from userNode
	userNode:exitApp(BusNum),
    %% Return with success (200) HTTP status code
    {200, {json, []}, State}.

%% POST REQUEST: Submit bus information once you get on a bus
post("/user", Req, State) ->
    %% Get POST body query string
    Post = leptus_req:body_qs(Req),

    %% Bus record ID is based on timestamp
    {MegaS, S, MicroS} = erlang:now(),
    Id = list_to_binary(
        integer_to_list(MegaS) ++
        integer_to_list(S) ++
        integer_to_list(MicroS)
    ),

    %% Create Latest Arrival Timestamp based on current time
    {_={_,_,_},_={Hour,Minutes,Seconds}} = calendar:now_to_local_time(erlang:now()),
    Latest_Arrival_Timestamp = list_to_binary(
                                    integer_to_list(Hour)++":"++
                                    integer_to_list(Minutes)++":"++
                                    integer_to_list(Seconds)),

    %% Get desired fields from POST
    {<<"bus_name">>, Bus_Name} = lists:keyfind(<<"bus_name">>, 1, Post),
    {<<"latest_arrival_place">>, Latest_Arrival_Place} = lists:keyfind(<<"latest_arrival_place">>, 1, Post),
    {<<"notifications_arrival">>, Notifications_Arrival} = lists:keyfind(<<"notifications_arrival">>, 1, Post),
    {<<"notifications_delay">>, Notifications_Delay} = lists:keyfind(<<"notifications_delay">>, 1, Post),
    {<<"notifications_early">>, Notifications_Early} = lists:keyfind(<<"notifications_early">>, 1, Post),
    {<<"fullness">>, Fullness} = lists:keyfind(<<"fullness">>, 1, Post),
    {<<"smelliness">>, Smelliness} = lists:keyfind(<<"smelliness">>, 1, Post),
    {<<"comments">>, Comments} = lists:keyfind(<<"comments">>, 1, Post),

    %% Create a record on new data available
    Bus_Record = #bus_records{
        id = Id,
        latest_arrival_timestamp = Latest_Arrival_Timestamp,
        bus_name = Bus_Name,
        latest_arrival_place = Latest_Arrival_Place,
        notifications_arrival = Notifications_Arrival,
        notifications_delay = Notifications_Delay,
        notifications_early = Notifications_Early,
        fullness = Fullness,
        smelliness = Smelliness,
        comments = Comments
    },
    %% Update bus record on Mnesia table and share info with other nodes in group
    userNode:update_BusRecord(Bus_Record),

    %% Return success
    {200, {json, Post}, State}.

%% End
terminate(_Reason, _Route, _Req, _State) ->
    ok.
