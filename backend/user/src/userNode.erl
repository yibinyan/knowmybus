-module(userNode).
-behaviour(gen_server).

-export([start_link/0, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).
-export([install/0, addUser/2, removeUser/1, addAllUsers/1]).
-export([select_BusRoute/2, traverse_table_and_show/0]).
-export([update_BusRecord/1, listify_BusRecords/0, insert_BusRecord/1, exitApp/1]).

%% ========================================================================================
%% Include files
%% ========================================================================================
-include("bus_record.hrl").
-include_lib("stdlib/include/qlc.hrl").


%% ========================================================================================
%% Records
%% ========================================================================================
-record(userInfo, {name,
				   info = []}).


%% ========================================================================================
%% Basic init support
%% ========================================================================================
%% Start_link
start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


%% Stop the node
stop() ->
	%% Delete Mnesia table and schema
	{atomic, ok} = mnesia:delete_table(bus_records),
	{atomic, ok} = mnesia:delete_table(userInfo),

	mnesia:stop(),
	mnesia:delete_schema(node()),
	%% Exit normally
	gen_server:call({local, ?MODULE}, stop).

%% ========================================================================================
%% Callback functions
%% ========================================================================================
init([]) ->
	%% wait for table to be loaded from disc
	mnesia:wait_for_tables([userInfo], 2000),
	%% Start group communication module
	groupCommunication:startMsgReceiver(),
	{ok, []}.

handle_call(_Call, _From, State) ->
    {noreply, State}.

handle_cast(_Cast, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

terminate(_Reason, _State) ->
    ok.

%% ========================================================================================
%% Global support functions
%% ========================================================================================
%% Setup node by initializing schema and table
install() ->
	%% Stop Mnesia t o create schema
	application:stop(mnesia),

    %% Define static directory for application (front-end files)
    Opts = [{static_dir, {'_', {priv_dir, ?MODULE, "templates"}}}],
    %% Start Leptus listener and set it to route GUI requests to src/gui_request_handler.erl
	leptus:start_listener(http, [{'_', [{gui_request_handler, undef}]}], Opts),

	%% Create schema before creating table
	ok = mnesia:create_schema([node()]),

	%% Create table to keep track of all members belonging to same
	%% busNum (contents are local to this node)
	application:start(mnesia),
	{atomic, ok} = mnesia:create_table(userInfo,
						[{attributes, record_info(fields, userInfo)},
						 {disc_copies, [node()]},
						 {local_content, true}
						]),
    %% Create another mnesia table based on bus_record (defined in src/bus_record.hrl)
    {atomic, ok} = mnesia:create_table(bus_records, 
   					   [{attributes, record_info(fields, bus_records)},
      					{disc_copies, [node()]},
      					{local_content, true}
      				   ]),

	%% Connect to bootstrap server by setting cookie to knowmybus
	true = erlang:set_cookie(node(), 'knowmybus'),
	ServerList = ['kmbBootstrapServMain@ec2-52-4-35-137.compute-1.amazonaws.com',
				  'kmbBootstrapServA@ec2-52-4-35-137.compute-1.amazonaws.com',
			 	  'kmbBootstrapServB@ec2-52-4-35-137.compute-1.amazonaws.com'],
	ok = connectNode(ServerList),
	io:format("~n Install complete ~n").


%% Event to add new user into userInfo Mnesia table
addUser(Name, Info) ->
	F = fun() ->
		mnesia:write(#userInfo{name=Name, info=Info})
	end,
	mnesia:activity(transaction, F).


%% Event to remove user from userInfo Mnesia table
removeUser(Name) ->
	F = fun() ->
		mnesia:delete({userInfo, Name})
	end,
	mnesia:activity(transaction, F).


%% Add the list of all users in the same bus group
addAllUsers(List) ->
	%% Add the list to local Mnesia table
	F = fun() ->
		update_table(List)
	end,
	mnesia:activity(transaction, F).

%% ========================================================================================
%% Interaction with User Interface
%% ========================================================================================
%% Select bus route - called from application
select_BusRoute(BusNum, Info) ->
	%% Clear Mnesia tables and schema
	{atomic, ok} = mnesia:clear_table(bus_records),
	{atomic, ok} = mnesia:clear_table(userInfo),

	%% Make RPC call to join bus group
	ServerList = ['kmbBootstrapServMain@ec2-52-4-35-137.compute-1.amazonaws.com',
				  'kmbBootstrapServA@ec2-52-4-35-137.compute-1.amazonaws.com',
			 	  'kmbBootstrapServB@ec2-52-4-35-137.compute-1.amazonaws.com'],
	Ret = make_rpc(ServerList, bootstrapServ, join_group, [BusNum, node(), Info]),
	Ret.


%% Update bus record Mnesia table after sending the new info
%% to members in the bus group
update_BusRecord(BusRecord) ->
	%% Get list of members in group
	Fun = fun() ->
		qlc:e(
			qlc:q([N || #userInfo{name=N, info=_} <- mnesia:table(userInfo)])
			)
	end,
	{atomic, UserInfoList} = mnesia:transaction(Fun),
	%% Send bus record to all members in group
	{ok} = groupCommunication:sendGroupMessage(BusRecord, UserInfoList),
	%% If sucessful, update Bus record table with new record
	F = fun() ->
		mnesia:write(bus_records, BusRecord, write)
	end,
	mnesia:activity(transaction, F).


%% Get a list of all the records gathered in bus record Mnesia table so far
listify_BusRecords() ->
	%% Sort (ascending order) the bus records on id (key) which is
	%% the first parameter in this table
	Fun = fun() ->
		qlc:e(
			qlc:keysort(1,
				qlc:q([X || X <- mnesia:table(bus_records)]),
				[{order, ascending}]
				)
			 )
	end,
	{atomic, RecordList} = mnesia:transaction(Fun),
	%% Return Record List
	RecordList.


%% Insert received bus record into local bus record Mnesia table
insert_BusRecord(BusRecord) ->
	F = fun() ->
		mnesia:write(bus_records, BusRecord, write)
	end,
	mnesia:activity(transaction, F).


%% Called when user selects a different bus
exitApp(BusNum) ->
	%% Make RPC call to exit bus group
	ServerList = ['kmbBootstrapServMain@ec2-52-4-35-137.compute-1.amazonaws.com',
				  'kmbBootstrapServA@ec2-52-4-35-137.compute-1.amazonaws.com',
			 	  'kmbBootstrapServB@ec2-52-4-35-137.compute-1.amazonaws.com'],
	make_rpc(ServerList, bootstrapServ, exit_group, [BusNum, node()]),

	%% Clear Mnesia tables and schema
	{atomic, ok} = mnesia:clear_table(bus_records),
	{atomic, ok} = mnesia:clear_table(userInfo).

%% ========================================================================================
%% Private support modules
%% ========================================================================================
%% Fault tolerant RPC call
make_rpc([], _, _, _) ->
	%% TODO: use delay instead of exit?
	404;
	%exit(normal);
make_rpc([Server | RestServerList], ServerModule, ServerFunction, Args) ->
	try rpc:call(Server, ServerModule, ServerFunction, Args) of 
		{badrpc, _} ->
			make_rpc(RestServerList, ServerModule, ServerFunction, Args);
		_ ->
			200
	catch
		_ ->
		 	supervisor:terminate_child(user_supervisor, userNodeID)
	end.


%% Write the contents of server response to Mnesia table
update_table([]) ->
	ok;
update_table([{N, I} | Rest]) ->
	mnesia:write(#userInfo{name=N, info=I}),
	update_table(Rest).


%% If no server can be connected exit
connectNode([]) ->
	%supervisor:terminate_child(user_supervisor, userNodeID);
	io:format(" Unable to connect to server ~n"),
	{error};
%% Connect to one of the servers
connectNode([Server | RestServerList]) ->
	try net_kernel:connect_node(Server) of 
		true ->
			ok;
		false ->
			connectNode(RestServerList)
	catch
		_ ->
		 	exit(normal)
	end.


%% ========================================================================================
%% Debug support module
%% ========================================================================================
%% Print out table contents
traverse_table_and_show()->
    Iterator =  fun(Rec,_)->
                    io:format("~p~n",[Rec]),
                    []
                end,
    case mnesia:is_transaction() of
        true -> mnesia:foldl(Iterator,[],userInfo);
        false -> 
            Exec = fun({Fun,Tab}) -> mnesia:foldl(Fun, [],Tab) end,
            mnesia:activity(transaction,Exec,[{Iterator,userInfo}],mnesia_frag)
    end.
