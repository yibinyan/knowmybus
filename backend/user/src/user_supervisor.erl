-module(user_supervisor).
-behaviour(supervisor).

-export([start_link/0, init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
	MaxRestart = 6,
	MaxTime = 5000,
	{ok, {{one_for_one, MaxRestart, MaxTime}, 	%% {RestartStrategy, MaxRestart, MaxTime} format
		  [{userNodeID,
		   {userNode, start_link, []},			%% StartFunc in {M,F,A} format
		   permanent,							%% Restart
		   5000,								%% Max shutdown time
		   worker,								%% Child is supervisor
		   [userNode]
		  }]}}.
